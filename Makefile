#  Makefile
#  
#  Copyright 2014-2015 Karl Lindén <karl.j.linden@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

TEXFILES = khat.tex
TEXINPUTS = bibl.bib

TEXI2PDF ?= texi2pdf

CLEANFILES += $(TEXFILES:.tex=.aux)
CLEANFILES += $(TEXFILES:.tex=.bbl)
CLEANFILES += $(TEXFILES:.tex=.blg)
CLEANFILES += $(TEXFILES:.tex=-blx.bib)
CLEANFILES += $(TEXFILES:.tex=.lof)
CLEANFILES += $(TEXFILES:.tex=.log)
CLEANFILES += $(TEXFILES:.tex=.lol)
CLEANFILES += $(TEXFILES:.tex=.lot)
CLEANFILES += $(TEXFILES:.tex=.nav)
CLEANFILES += $(TEXFILES:.tex=.out)
CLEANFILES += $(TEXFILES:.tex=.pdf)
CLEANFILES += $(TEXFILES:.tex=.run.xml)
CLEANFILES += $(TEXFILES:.tex=.snm)
CLEANFILES += $(TEXFILES:.tex=.toc)
CLEANFILES += $(TEXFILES:.tex=.url)

# Let images be part here so that they are not removed as intermediate
# results
all: $(TEXFILES:tex=pdf)
	

%.pdf: %.tex $(TEXINPUTS)
	$(TEXI2PDF) --batch $< -o $@

.PHONY: all clean
clean:
	-rm -f $(CLEANFILES)
